<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$data = json_decode(file_get_contents('php://input'), true);
if (empty($data) || !isset($data['action'])) {
    die('not allowed');
}

require_once 'core/init.php';
$service = new \classes\Service();

switch ($data['action']) {
    case 'reg':
        $result = $service->register($data);
        break;
    case 'auth':
        $result = $service->authorize($data);
        break;
    case 'exit':
        $result = $service->quit();
        break;
    case 'saveInfo':
        $result = $service->saveUserResults($data);
        break;
    case 'statistic':
        $result = $service->getUserStatistic();
        break;
    case 'getInfo':
        try{
            $result = $service->getActualInfo();
        }
        catch (Exception $e){}
        break;
    default:
        $result = [];
        break;
}


echo json_encode($result);
die();
