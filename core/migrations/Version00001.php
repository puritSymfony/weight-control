<?php

namespace migrations;

use classes\Database;

class Version00001
{
    private $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    /**
     * Создадим пустые таблицы
     */
    public function createUserTable()
    {
        $sql = "CREATE TABLE `user` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(100) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`updated` DATETIME NULL DEFAULT NULL,
	`created` DATETIME NULL DEFAULT NULL,
	`password` VARCHAR(100) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `Unique_email` (`email`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;";
        $this->db->runSql($sql);
    }

    /**
     * Выполнить 1
     */
    public function createProgramTable()
    {

        $sql = "CREATE TABLE `program` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`date` DATE NOT NULL,
	`weight` VARCHAR(5) NOT NULL,
	`user_id` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;";

        $this->db->runSql($sql);
    }

}