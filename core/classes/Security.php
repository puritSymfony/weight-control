<?php


namespace classes;


class Security
{
    private $salt = 'SanekOk2021';

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    public function checkUserPassword($userPassword, $inputPassword)
    {
        $inputPasswordHash = md5($inputPassword.$this->getSalt());
        return $inputPasswordHash === $userPassword;
    }

    /**
     * @return array|bool
     */
    public function getUser()
    {
        if(!isset($_SESSION['userInfo'])){
            return false;
        }

        return [
            'id' => $_SESSION['userInfo']['id'],
            'email' => $_SESSION['userInfo']['email'],
        ];


    }

    /**
     * @param $user
     */
    public function createUserSession($user)
    {
        $_SESSION['userInfo'] = $user;
    }

    /**
     * @return bool
     */
    public function removeUserSession()
    {
        unset($_SESSION['userInfo']);
        session_destroy();
        return true;
    }

}