<?php

namespace classes;

use PDO;

class Database
{
    private $_connection;
    private static $_instance; //The single instance
    private $config;


    /**
     * @return Database
     */
    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     *
     */
    private function initSettings()
    {
        $this->config = parse_ini_file(CORE_PATH . '/config/config.ini', true);
    }

    // Constructor
    private function __construct()
    {
        $this->initSettings();
        try {
            $this->_connection = new PDO('mysql:host=' . $this->config['host'] . ';dbname=' . $this->config['dbname'] . '', $this->config['user'], $this->config['pass']);
            $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            trigger_error("Failed to conencto to MySQL: " . $e->getMessage(),
                E_USER_ERROR);
            die();
        }
    }

    private function __clone()
    {
    }

    public function getConnection()
    {
        return $this->_connection;
    }


    /**
     * @param $sql
     * @param $params
     * @return array
     */
    public function runSafeSqlArray($sql, $params)
    {
        $sql = $this->_connection->prepare($sql);
        $final = $sql->execute($params);
        return $sql->fetchAll();
    }

    /**
     * @param $sql
     * @param $params
     * @return bool
     */
    public function runSafeSqlTransaction($sql, $params)
    {
        $this->_connection->beginTransaction();
        try {
            $this->runSafeSql($sql, $params);
        } catch (\PDOException $exception) {
            echo "<pre>";
            print_r($exception->getMessage());
            echo "</pre>";
            die();
            $this->_connection->rollBack();
            return false;
        }
        $this->_connection->commit();
        return true;
    }

    /**
     * @param $sql
     * @param $params
     * @return mixed
     */
    public function runSafeSql($sql, $params)
    {
        $needFetch = true;
        if (stripos($sql, 'insert') !== false || stripos($sql, 'update') !== false) {
            $needFetch = false;
        }


        /** @var \PDOStatement $sql */
        $sql = $this->_connection->prepare($sql);
        $final = $sql->execute($params);
        if ($needFetch) {
            $final = $sql->fetch();
        }

        return $final;
    }

    public function runSql($sql)
    {
        $this->_connection->exec($sql);
    }
}