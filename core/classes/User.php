<?php


namespace classes;


class User extends Base
{

    /**
     * @param $email
     * @return mixed
     */
    public function findUserByEmail($email)
    {
        $sql = 'SELECT * FROM user where email = :email';
        return $this->db->runSafeSql($sql, ['email' => $email]);
    }

    /**
     * Есть ли у пользователя данные о его записях
     * @param $userId
     * @param string $orderBy
     * @param string $order
     * @return mixed
     */
    public function getUserProgram($userId, $orderBy = 'date', $order = 'ASC')
    {
        $sql = 'SELECT * FROM `program` where user_id = :userId order by '. $orderBy .'  '. $order .';';
        return $this->db->runSafeSqlArray($sql, ['userId' => $userId]);
    }


    /**
     * Обновление программы по дате
     * @param $userId
     * @param $date
     * @param $weight
     * @return array
     */
    public function updateUserProgram($userId, $date, $weight)
    {
        $sql = 'UPDATE `program` SET weight = :weight where user_id = :userId and date = :current_date;';
        return $this->db->runSafeSql($sql, ['userId' => $userId, 'current_date' => $date, 'weight' => $weight]);
    }

    /**
     * @param $userId
     * @param $date
     * @return array
     */
    public function getUserProgramByDate($userId, $date)
    {
        $sql = 'SELECT * FROM `program` where user_id = :userId and date = :current_date;';
        return $this->db->runSafeSqlArray($sql, ['userId' => $userId, 'current_date' => $date]);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createUser($params)
    {
        $sql = 'INSERT INTO `user` (`username`, `email`, `password`) VALUES (:username, :email, :password);';
        if ($this->db->runSafeSql($sql, $params)) {
            return $this->findUserByEmail($params['email']);
        }
        return false;
    }


    /**
     * @param $params
     * @return bool
     */
    public function saveUserProgram($params)
    {

        $sql = "insert into `program` (`date`, `weight`, `user_id`) values ";
        $paramArray = array();
        $sqlArray = array();

        foreach ($params as $row) {
            $sqlArray[] = '(' . implode(',', array_fill(0, count($row), '?')) . ')';
            foreach ($row as $element) {
                $paramArray[] = $element;
            }
        }

        $sql .= implode(',', $sqlArray);

        $res = $this->db->runSafeSqlTransaction($sql, $paramArray);
        return $res;
    }


}