<?php


namespace classes;


class Service extends Base
{

    protected $security;
    protected $userRepository;

    public function __construct()
    {
        $this->security = new Security();
        $this->userRepository = new User();
        parent::__construct();
    }

    /**
     * @param $userInfo
     * @return mixed
     */
    private function createAndGetUser($userInfo)
    {
        $params = [
            'password' => md5($userInfo['password'] . $this->security->getSalt()),
            'username' => $userInfo['username'],
            'email' => $userInfo['email'],
        ];
        $this->userRepository->createUser($params);
        $user = $this->userRepository->findUserByEmail($userInfo['email']);
        $this->security->createUserSession($user);
        return $user;
    }

    /**
     * Регистрация
     * @param $userInfo
     * @return array
     */
    public function register($userInfo)
    {

        $user = $this->userRepository->findUserByEmail($userInfo['email']);

        $result = [
            'status' => false,
            'message' => 'Пользователь уже имеется',
        ];


        if (!$user) {
            $user = $this->createAndGetUser($userInfo);

            $result = [
                'status' => true,
                'message' => 'Пользователь создан',
                'userInfo' => [
                    'name' => $user['username'],
                    'email' => $user['email'],
                ],
            ];

        }

        return $result;
    }

    /**
     * Авторизация
     * @param $userInfo
     * @return array
     */
    public function authorize($userInfo)
    {
        $user = $this->userRepository->findUserByEmail($userInfo['email']);

        if (!$user) {
            return $this->returnJson(false, 'Пользователь не найден');
        }
        if ($this->security->checkUserPassword($user['password'], $userInfo['password'])) {
            $this->security->createUserSession($user);
        }

        return $this->returnJson(true, '', [
            'userInfo' => [
                'name' => $user['username'],
                'email' => $user['email'],
            ]
        ]);

    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getActualInfo()
    {
        $months = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
            "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"
        ];

        $user = $this->security->getUser();
        $userProgram = $this->userRepository->getUserProgramByDate($user['id'], date('Y-m-d'));

        $weightDate = 0;
        if (count($userProgram) > 0) {
            $weightDate = $userProgram[0]['weight'];
        }

        $month = date('n');
        return $this->returnJson(true, '', [
            'date' => (new \DateTime())->format('d') . ' ' . $months[$month - 1],
            'weight' => $weightDate
        ]);
    }


    /**
     * Выход с учетной записи
     * @return array
     */
    public function quit()
    {
        $result = $this->security->removeUserSession();
        return $this->returnJson($result);
    }


    /**
     * @param $info
     * @return array
     */
    public function saveUserResults($info)
    {
        $user = $this->security->getUser();

        if (!$user) {
            return $this->returnJson(false, 'Пользователь не авторизован');
        }

        $userPrograms = $this->userRepository->getUserProgram($user['id']);

        // Если есть заполненные старые даты - проверим, что они не совпадают с существующими
        $duplicates = [];
//        $needSaveMainData = true;
        $updateMainDate = false;

        foreach ($userPrograms as $program) {
            if ($program['date'] === $info['data']['main']['mainData']['date']) {
                $updateMainDate = true;
                break;
            }
        }

        if (isset($info['data']['additional']) && count($info['data']['additional']) > 1) {
            foreach ($userPrograms as $program) {
                foreach ($info['data']['additional'] as $newProgram) {
                    if ($program['date'] === $newProgram['date']) {
                        $duplicates[] = $newProgram['date'];
                    }
                }
            }
        }

        if (count($duplicates) >= 1) {
            return $this->returnJson(false, 'Следующие даты: ' . implode(', ', $duplicates) . '  были записаны ранее.');
        }




        if ($updateMainDate) {
            $this->userRepository->updateUserProgram($user['id'], $info['data']['main']['mainData']['date'], $info['data']['main']['mainData']['weight']);
        } else {
            $info['data']['additional'][] = [
                'date' => $info['data']['main']['mainData']['date'],
                'weight' => $info['data']['main']['mainData']['weight'],
            ];
        }

        $params = $info['data']['additional'];

        foreach ($params as &$param) {
            $param['user_id'] = $user['id'];
        }


        if (empty($params) && $updateMainDate) {
            return $this->returnJson(true, 'Данные обновлены');
        }

        $result = [
            'status' => false,
            'message' => 'Ошибка записи'
        ];

        if ($this->userRepository->saveUserProgram($params)) {
            $result = [
                'status' => true,
                'message' => 'Информация сохранена'
            ];
        }

        return $this->returnJson($result['status'], $result['message']);
    }


    /**
     * @return array
     */
    public function getUserStatistic()
    {
        $user = $this->security->getUser();
        $userPrograms = $this->userRepository->getUserProgram($user['id']);

        $resultData = [];

        try {
            $programsPeriod = $this->getDatesWithPeriod($userPrograms);
        } catch (\Exception $e) {
            return $this->returnJson(false, 'Ошибка вычета периодов ' . $e->getMessage());
        }

        foreach ($userPrograms as $userProgram) {
            $resultData[] = [
                'id' => $userProgram['id'],
                'date' => date('y.m.d', strtotime($userProgram['date'])),
                'dateToCompare' => $userProgram['date'],
                'weight' => $userProgram['weight'],
                'period' => $programsPeriod[$userProgram['id']]['key'],
                'order' => $programsPeriod[$userProgram['id']]['order']
            ];
        }

        return $this->returnJson(true, '', ['data' => $resultData]);
    }


    /**
     * @param $array
     * @return array
     * @throws \Exception
     */
    private function getDatesWithPeriod($array)
    {

        $week = (new \DateTime())->modify('-1 week');
        $twoWeek = (new \DateTime())->modify('-2 weeks');
        $month = (new \DateTime())->modify('-1 month');
        $halfYear = (new \DateTime())->modify('-6 months');
        $year = (new \DateTime())->modify('-1 year');

        $datePeriods = [
            'week' => [
                'date' => $week,
                'dateString' => $week->format('y.m.d'),
                'order' => 0
            ],
            'twoWeek' => [
                'date' => $twoWeek,
                'dateString' => $twoWeek->format('y.m.d'),
                'order' => 1
            ],
            'month' => [
                'date' => $month,
                'dateString' => $month->format('y.m.d'),
                'order' => 2
            ],
            'halfYear' => [
                'date' => $halfYear,
                'dateString' => $halfYear->format('y.m.d'),
                'order' => 3
            ],
            'year' => [
                'date' => $year,
                'dateString' => $year->format('y.m.d'),
                'order' => 4
            ],
        ];

        $reversed = array_reverse($array);

        $periods = [];
        foreach ($reversed as $value) {
            $res = $this->defineDatePeriod($datePeriods, $value['date']);
            if (!empty($res)) {
                $periods[$value['id']] = [
                    'key' => $res['key'],
                    'order' => $res['order']
                ];
                continue;
            }
        }

        return $periods;
    }


    /**
     * Возвращает ключ периода которому принадлежит текущая дата программы
     * @param $periods
     * @param $second
     * @return array
     * @throws \Exception
     */
    private function defineDatePeriod($periods, $second)
    {
        foreach ($periods as $key => $period) {
            if ($this->checkTwoDates($period['date'], $second)) {
                return [
                    'key' => $key,
                    'order' => $period['order']
                ];
            }
        }
        return [];
    }

    /**
     * Сравнивает 2 даты
     * @param $firstDate
     * @param $secondDate
     * @return bool
     * @throws \Exception
     */
    private function checkTwoDates($firstDate, $secondDate)
    {
        $comparedDate = new \DateTime($secondDate);
        if ($comparedDate > $firstDate) {
            return true;
        }
        return false;
    }

    /**
     * @param $status
     * @param string $message
     * @param array $additional
     * @return array
     */
    public function returnJson($status, $message = '', $additional = [])
    {
        $result = [
            'status' => $status,
            'message' => $message,
        ];

        return array_merge($result, $additional);
    }

}