<?php
session_start ();

define('CORE_PATH', realpath(__DIR__) );

spl_autoload_register(function($className)
{
    $className=str_replace("\\","/",$className);
    $class=CORE_PATH."/{$className}.php";
    include_once($class);
});
