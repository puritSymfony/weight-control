let path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
var webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: './assets/js/index.js',
    output: {
        path: __dirname + '/build',
        filename: 'bundle.js',
        publicPath: '/build/',
        chunkFilename: '[name].[contenthash].js'
    },

    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    plugins: [require('babel-plugin-syntax-dynamic-import')]
                }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            }
        ],

    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    }
};