<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width"/>
    <title>test</title>
</head>
<body>

<div id="vueApp" data-email="<?= isset($_SESSION['userInfo']['email']) ? $_SESSION['userInfo']['email'] : false ?>"
     data-name="<?= isset($_SESSION['userInfo']['username']) ? $_SESSION['userInfo']['username'] : false ?>"></div>

<script src="build/bundle.js?9"></script>
</body>
</html>

