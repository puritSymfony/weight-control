export default {
    methods: {
        changeComponent: function (route, component) {
            this.$router.push({name: route}).catch(()=>{});
            this.$store.commit('setComponent', component);
        }
    }
};