import Vue from 'vue';
import Vuex from 'vuex';
import app from './vue/app.vue';
import VueRouter from 'vue-router'
import Mixin from './mixins/globalMixin.js'
import '../css/main.css';


Vue.config.devtools = false;
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.prototype.registrationAction = 'reg';
Vue.prototype.authorizationAction = 'auth';
Vue.prototype.exitAction = 'exit';
Vue.mixin(Mixin);

const Info = ()=> import('./vue/info.vue');
const MainPage = ()=> import('./vue/mainPage.vue');
const UserMenu = ()=> import('./vue/userMenu.vue');
const Statistic = ()=> import('./vue/statistic.vue');
// const Fill = ()=> import('./vue/fill.vue');

const routes = [
    {
        name:'info',
        path: '/info',
        component: Info,
    },
    {
        name:'userMenu',
        path: '/userMenu',
        component: UserMenu,
    },
    {
        name:'main',
        path: '/main',
        component: MainPage,
    },
    {
        name:'statistic',
        path: '/statistic',
        component: Statistic,
    }
    // { path: '/fill', component: Bar }
];

const router = new VueRouter({
    routes // сокращённая запись для `routes: routes`
});

const store = new Vuex.Store({
    state: {
        user: null,
        component: 'MainPage',
        userStatistics: []
    },
    getters: {
        getUser: state => {
            return state.user;
        },
        getUserStatistics: state => {
            return state.userStatistics;
        },
        getComponent: state => {
            return state.component;
        },
    },
    mutations: {
        setUser(state, info){
            state.user = info;
        },
        setUserStatistics(state, info){
            state.userStatistics = info;
        },
        setComponent(state, component){
            state.component = component;
        }
    }
});

let vueApp = new Vue({
    el: '#vueApp',
    store,
    router,
    template: '<app/>',
    components: {app}
});